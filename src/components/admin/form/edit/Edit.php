<?php

declare(strict_types=1);

namespace Skadmin\Transport\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Transport\BaseControl;
use Skadmin\Transport\Doctrine\Transport\Transport;
use Skadmin\Transport\Doctrine\Transport\TransportFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\Transport\Factory\TransportSettingsFactory;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private TransportSettingsFactory $transportSettings;
    private TransportFacade          $facade;
    private LoaderFactory            $webLoader;
    private Transport                $transport;

    public function __construct(?int $id, TransportSettingsFactory $transportSettings, TransportFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);

        $this->transportSettings = $transportSettings;
        $this->facade            = $facade;
        $this->webLoader         = $webLoader;

        $this->transport = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->transport->isLoaded()) {
            return new SimpleTranslation('transport.edit.title - %s', $this->transport->getName());
        }

        return 'transport.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->transport         = $this->transport;
        $template->transportSettings = $this->transportSettings;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.transport.edit.name')
            ->setRequired('form.transport.edit.name.req');
        $form->addCheckbox('isActive', 'form.transport.edit.is-active')
            ->setDefaultValue(true);
        $form->addTextArea('content', 'form.transport.edit.content');
        $form->addTextArea('contentEmail', 'form.transport.edit.content-email');
        $form->addImageWithRFM('imagePreview', 'form.transport.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.transport.edit.send');
        $form->addSubmit('sendBack', 'form.transport.edit.send-back');
        $form->addSubmit('back', 'form.transport.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->transport->isLoaded()) {
            return [];
        }

        return [
            'name'         => $this->transport->getName(),
            'content'      => $this->transport->getContent(),
            'contentEmail' => $this->transport->getContentEmail(),
            'isActive'     => $this->transport->isActive(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->transport->isLoaded()) {
            $transport = $this->facade->update(
                $this->transport->getId(),
                $values->name,
                $values->isActive,
                $values->content,
                $values->contentEmail,
                $identifier
            );
            $this->onFlashmessage('form.transport.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $transport = $this->facade->create(
                $values->name,
                $values->isActive,
                $values->content,
                $values->contentEmail,
                $identifier
            );
            $this->onFlashmessage('form.transport.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $transport->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }
}
