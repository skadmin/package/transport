<?php

declare(strict_types=1);

namespace Skadmin\Transport\Doctrine\Transport;

use App\Model\Doctrine\Traits;
use Doctrine\DBAL\Types\Types;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;
use function trim;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Transport
{
    use Entity\BaseEntity;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;

    #[ORM\Column(type: Types::TEXT)]
    private string $contentEmail;

    public function update(string $name, bool $isActive, string $content, string $contentEmail, ?string $imagePreview) : void
    {
        $this->name         = $name;
        $this->isActive     = $isActive;
        $this->content      = $content;
        $this->contentEmail = $contentEmail;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function getContentEmail() : string
    {
        return $this->contentEmail;
    }

}
