<?php

declare(strict_types=1);

namespace Skadmin\Transport\Doctrine\Transport;

use SkadminUtils\DoctrineTraits\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class TransportFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Transport::class;
    }

    public function create(string $name, bool $isActive, string $content, string $contentEmail, ?string $imagePreview) : Transport
    {
        return $this->update(null, $name, $isActive, $content, $contentEmail, $imagePreview);
    }

    public function update(?int $id, string $name, bool $isActive, string $content, string $contentEmail, ?string $imagePreview) : Transport
    {
        $transport = $this->get($id);
        $transport->update($name, $isActive, $content, $contentEmail, $imagePreview);

        $this->em->persist($transport);
        $this->em->flush();

        return $transport;
    }

    public function get(?int $id = null) : Transport
    {
        if ($id === null) {
            return new Transport();
        }

        $transport = parent::get($id);

        if ($transport === null) {
            return new Transport();
        }

        return $transport;
    }

    /**
     * @return Transport[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }
}
