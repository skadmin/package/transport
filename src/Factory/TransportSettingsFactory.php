<?php

declare(strict_types=1);

namespace SkadminUtils\Transport\Factory;

class TransportSettingsFactory
{
    private bool $allowContentEmail;

    public function __construct(bool $allowContentEmail)
    {
        $this->allowContentEmail = $allowContentEmail;
    }

    public function isAllowContentEmail() : bool
    {
        return $this->allowContentEmail;
    }

}
