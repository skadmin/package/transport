<?php

declare(strict_types=1);

namespace SkadminUtils\Transport\DI;

use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use SkadminUtils\Transport\Factory\TransportSettingsFactory;

class TransportExtension extends CompilerExtension
{
    public function getConfigSchema() : Schema
    {
        return Expect::structure([
            'allowContentEmail' => Expect::bool(false),
        ]);
    }

    public function loadConfiguration() : void
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('transportSettingsFactory'))
            ->setClass(TransportSettingsFactory::class)
            ->setArguments([
                $this->config->allowContentEmail,
            ]);
    }
}
