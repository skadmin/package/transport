<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211226053518 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'transport.overview', 'hash' => '925dd06dd689964c0fcc60ef93d727bf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy doprav', 'plural1' => '', 'plural2' => ''],
            ['original' => 'transport.overview.title', 'hash' => '9f30a771c7d2482722065c1239ad0ba9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy doprav|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.transport.overview.is-active', 'hash' => '5e2d385de5f9d8d3e716a4fc90ae70df', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.transport.overview.action.new', 'hash' => '0dc53e4e02ad00ead9aaf9b17eb1524b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit typ dopravy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.transport.overview.name', 'hash' => '4d39cee96e4229bb72dac1f928ed91f9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.transport.overview.action.edit', 'hash' => '50b0039b864c95677295f7dfd26f47fb', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'transport.edit.title', 'hash' => 'e02a24fd0ee36421103bf7697110a8f1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení typu dopravy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.name', 'hash' => '5f74425b552836ac49bdea5767f662be', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.name.req', 'hash' => 'fc52b04c31580e9fee5b1cc3976f858b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.content', 'hash' => 'ab804705e33ca67376a02387e1718903', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.image-preview', 'hash' => '666bf4030e93ce4481008bd64ded060d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Logo', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.image-preview.rule-image', 'hash' => '5e1aedbad2345c7e861eb8aa28e4e88a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.is-active', 'hash' => '1998adb3e81d929c860dd7829c771539', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.send', 'hash' => '0a009b10acabdcf29a4a49626c988cfe', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.send-back', 'hash' => 'cb7d42714e9c0037186c033eec0aba7f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ulož a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.back', 'hash' => 'd96a5ffd037f59aeeddf909d51da9754', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.flash.success.create', 'hash' => '642ead12eeb58e4164ef255369aa9f28', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ dopravy byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'transport.edit.title - %s', 'hash' => 'bcf12a4e0cff65e1dc378d624aa40a04', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace typu dopravy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.transport.edit.flash.success.update', 'hash' => '551187ff2744fc049fa4e26303265938', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ dopravy byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
